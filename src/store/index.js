import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

Vue.use(Vuex)

// 状態Authと状態BoardをVuexのstateで一元管理できるように定義する
const state = {
  auth: {
    token: null,
    unserId: null
  },
  board: {
    lists: []
  }
}

export default new Vuex.Store({
  // 定義したstateをstateオプションに指定
  state,
  getters,
  actions,
  mutations,
  strict: process.env.NODE_ENV !== 'production'
})
