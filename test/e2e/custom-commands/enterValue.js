// input要素へのキーボード入力をエミュレートするカスタムコマンド
exports.commnd = function (selector, value) {
  return this.clearValue(selector)
    .setValue(selector, value)
    .trigger(selector, 'keyup', 13)
}
